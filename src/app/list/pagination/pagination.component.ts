import { CommonModule } from '@angular/common';
import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
} from '@angular/core';
import { RouterLink } from '@angular/router';

type PageItem = {
    num: number;
    isHiddenSection?: boolean;
};

@Component({
    selector: 'app-pagination',
    standalone: true,
    imports: [RouterLink, CommonModule],
    templateUrl: './pagination.component.html',
    styleUrl: './pagination.component.scss',
})
export class PaginationComponent implements OnInit, OnChanges {
    @Input() path!: string;
    @Input() query!: { [key: string]: string };
    @Input() currentPage!: number;
    @Input() totalPages!: number;
    @Input() visiblePages!: number;

    @Output() pageChanged = new EventEmitter<number>();

    pages: PageItem[] = [];

    ngOnInit(): void {
        console.log('on init');
        this.calculatePages();
    }

    get prevQuery() {
        return {
            ...this.query,
            page: this.currentPage > 1 ? this.currentPage - 1 : 1,
        };
    }

    get nextQuery() {
        return {
            ...this.query,
            page:
                this.currentPage < this.totalPages
                    ? this.currentPage + 1
                    : this.currentPage,
        };
    }

    pageQuery(page: number) {
        return { ...this.query, page: page - 1 };
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.calculatePages();
    }

    calculatePages() {
        if (!this.totalPages) {
            this.pages = [];
            return;
        }
        if (this.totalPages <= this.visiblePages) {
            this.pages = new Array(this.totalPages)
                .fill(1)
                .map((v, i) => ({ num: v + i }));
        } else {
            const centerLength = this.visiblePages - 2;
            const centerPages = new Array(centerLength)
                .fill(0)
                .map((v, i) => v + i);
            let centerStart = this.currentPage - Math.floor(centerLength / 2);
            if (centerStart < 1) centerStart = 1;
            let centerEnd = centerStart + (centerLength - 1);
            if (centerEnd > this.totalPages) {
                centerStart -= centerEnd - this.totalPages;
                centerEnd = centerStart + (centerLength - 1);
            }
            let newPages = [
                ...(centerStart === 1 ? [] : [{ num: 1 }]),
                ...(centerStart <= 2
                    ? []
                    : [{ num: 0, isHiddenSection: true }]),
                ...centerPages.map((v) => ({ num: v + centerStart })),
                ...(centerEnd + 1 >= this.totalPages
                    ? []
                    : [{ num: 0, isHiddenSection: true }]),
                ...(centerEnd === this.totalPages
                    ? []
                    : [{ num: this.totalPages }]),
            ];
            this.pages = newPages;
        }
    }
}
