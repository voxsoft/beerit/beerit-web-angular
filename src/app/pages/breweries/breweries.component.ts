import { Component, inject } from '@angular/core';
import { ListLoadServiceService } from '../services/list-load-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Brewery } from '../../models/Brewery.model';
import { PageTitleComponent } from '../../layout/page-title/page-title.component';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from '../../list/pagination/pagination.component';

@Component({
    selector: 'app-breweries',
    standalone: true,
    imports: [CommonModule, PageTitleComponent, PaginationComponent],
    providers: [ListLoadServiceService],
    templateUrl: './breweries.component.html',
    styleUrl: './breweries.component.scss',
})
export class BreweriesComponent {
    listService = inject(ListLoadServiceService);
    route = inject(ActivatedRoute);
    router = inject(Router);
    items$: Observable<Brewery[]> = this.listService.reviews$;
    totalPages$: Observable<number> = this.listService.totalPages$;
    perPage = 12;
    page = 0;

    ngOnInit(): void {
        this.route.queryParamMap.subscribe((params) => {
            const pageParam = params.get('page');
            this.page = pageParam ? parseInt(pageParam) : this.page;
            this.listService.loadList('/entity/Brewery/list', {
                page: this.page,
                perPage: this.perPage,
            });
        });
    }

    pageChanged(page: number) {
        this.router.navigate(['/'], {
            queryParams: { page: page },
        });
    }
}
