import { Component, inject } from '@angular/core';
import { ListLoadServiceService } from '../services/list-load-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BeerStyle } from '../../models/BeerStyle.model';
import { CommonModule } from '@angular/common';
import { PageTitleComponent } from '../../layout/page-title/page-title.component';
import { PaginationComponent } from '../../list/pagination/pagination.component';

@Component({
    selector: 'app-beer-styles',
    standalone: true,
    imports: [CommonModule, PageTitleComponent, PaginationComponent],
    providers: [ListLoadServiceService],
    templateUrl: './beer-styles.component.html',
    styleUrl: './beer-styles.component.scss',
})
export class BeerStylesComponent {
    listService = inject(ListLoadServiceService);
    route = inject(ActivatedRoute);
    router = inject(Router);
    items$: Observable<BeerStyle[]> = this.listService.reviews$;
    totalPages$: Observable<number> = this.listService.totalPages$;
    perPage = 12;
    page = 0;

    ngOnInit(): void {
        this.route.queryParamMap.subscribe((params) => {
            const pageParam = params.get('page');
            this.page = pageParam ? parseInt(pageParam) : this.page;
            this.listService.loadList('/entity/BeerStyle/list', {
                page: this.page,
                perPage: this.perPage,
            });
        });
    }

    pageChanged(page: number) {
        this.router.navigate(['/'], {
            queryParams: { page: page },
        });
    }
}
