import { TestBed } from '@angular/core/testing';

import { ReviewsService } from './reviews.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

let httpSpy: jasmine.SpyObj<HttpClient>;

describe('ReviewsService', () => {
  let service: ReviewsService;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule({
      providers: [ReviewsService, {provide: HttpClient, useValue: spy}]
    });
    service = TestBed.inject(ReviewsService);
    httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch reviews', (done) => {
    const stubValue = {items: [], totalPages: 0};
    httpSpy.get.and.returnValue(new Observable((subscriber) => {
      subscriber.next(stubValue)
    }));
    service.getReviews({ page: 0 }).subscribe({
      next: (data) => {
        expect(data).toEqual(stubValue);
        done();
      },
      error: done.fail
    });
    expect(httpSpy.get.calls.count()).withContext('one call').toBe(1);
  })
});
