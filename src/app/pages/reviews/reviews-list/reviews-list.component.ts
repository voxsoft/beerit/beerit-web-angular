import { Component, OnInit, inject } from '@angular/core';
import { ReviewsService } from '../reviews.service';
import { CommonModule } from '@angular/common';
import { BeerCardComponent } from '../beer-card/beer-card.component';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BeerReview } from '../../../models/BeerReview';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationComponent } from '../../../list/pagination/pagination.component';
import { PageTitleComponent } from '../../../layout/page-title/page-title.component';

@Component({
    selector: 'app-reviews-list',
    standalone: true,
    imports: [
        CommonModule,
        BeerCardComponent,
        PaginationComponent,
        PageTitleComponent,
    ],
    providers: [ReviewsService],
    templateUrl: './reviews-list.component.html',
    styleUrl: './reviews-list.component.scss',
})
export class ReviewsListComponent implements OnInit {
    reviewsService = inject(ReviewsService);
    route = inject(ActivatedRoute);
    router = inject(Router);
    beers$: Observable<BeerReview[]> = this.reviewsService.reviews$;
    totalPages$: Observable<number> = this.reviewsService.totalPages$;
    perPage = 12;
    page = 0;

    ngOnInit(): void {
        this.route.queryParamMap.subscribe((params) => {
            const pageParam = params.get('page');
            this.page = pageParam ? parseInt(pageParam) : this.page;
            this.reviewsService.loadReviews({
                page: this.page,
                perPage: this.perPage,
            });
        });
    }

    pageChanged(page: number) {
        this.router.navigate(['/'], {
            queryParams: { page: page },
        });
    }
}
