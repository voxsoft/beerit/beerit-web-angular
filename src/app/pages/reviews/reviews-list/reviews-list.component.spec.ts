import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewsListComponent } from './reviews-list.component';
import { ActivatedRoute, provideRouter } from '@angular/router';
import { ReviewsService } from '../reviews.service';
import { Observable, of } from 'rxjs';

// class HeroDetailServiceSpy {
//   testHero: Hero = { ...testHero };

//   /* emit cloned test hero */
//   getHero = jasmine
//     .createSpy('getHero')
//     .and.callFake(() => asyncData(Object.assign({}, this.testHero)));

//   /* emit clone of test hero, with changes merged in */
//   saveHero = jasmine
//     .createSpy('saveHero')
//     .and.callFake((hero: Hero) => asyncData(Object.assign(this.testHero, hero)));
// }

describe('ReviewsListComponent', () => {
  let component: ReviewsListComponent;
  let fixture: ComponentFixture<ReviewsListComponent>;
  let service: ReviewsService;

  beforeEach(async () => {

    const ReviewsServiceSpy = jasmine.createSpyObj('ReviewsService', ['getReviews']);
    const result = ReviewsServiceSpy.getReviews.and.returnValue(of({items: [], totalPages: 0}));
    
    await TestBed.configureTestingModule({
      imports: [ReviewsListComponent],
      providers: [
        provideRouter([])
      ]
    }).overrideComponent(ReviewsListComponent, {
      set: { providers: [{ 
        provide: ReviewsService, 
        useValue: ReviewsServiceSpy 
      }]}
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ReviewsListComponent);
    component = fixture.componentInstance;
    service = fixture.debugElement.injector.get(ReviewsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
