import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-beer-card',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './beer-card.component.html',
  styleUrl: './beer-card.component.scss'
})
export class BeerCardComponent {
  @Input() beer: any;

}
