import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerCardComponent } from './beer-card.component';
import { provideRouter } from '@angular/router';

describe('BeerCardComponent', () => {
  let component: BeerCardComponent;
  let fixture: ComponentFixture<BeerCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        BeerCardComponent,
        provideRouter([])
      ]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BeerCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.beer = {name: '', image: null, description: '', abv: 0, ibu: 0}
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
