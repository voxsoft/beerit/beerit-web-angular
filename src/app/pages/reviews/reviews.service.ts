import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.development';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ReviewsService {
    reviews$ = new BehaviorSubject([]);
    totalPages$ = new BehaviorSubject(0);

    constructor(private http: HttpClient) {}

    loadReviews({ page, perPage }: { page: number; perPage: number }) {
        if (perPage === 0) {
            this.reviews$.next([]);
            this.totalPages$.next(0);
        }
        this.http
            .get<any>(`${environment.apiHost}/beer/tested/list`, {
                params: { pagination: JSON.stringify({ page, perPage }) },
            })
            .subscribe((res) => {
                const total = res.totalItems
                    ? Math.ceil(res.totalItems / perPage)
                    : 0;
                this.reviews$.next(res.items ? res.items : []);
                this.totalPages$.next(total);
            });
    }
}
