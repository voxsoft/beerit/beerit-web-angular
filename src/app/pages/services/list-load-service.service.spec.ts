import { TestBed } from '@angular/core/testing';

import { ListLoadServiceService } from './list-load-service.service';

describe('ListLoadServiceService', () => {
  let service: ListLoadServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListLoadServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
