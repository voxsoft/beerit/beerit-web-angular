import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ReviewsListComponent } from './pages/reviews/reviews-list/reviews-list.component';
import { BreweriesComponent } from './pages/breweries/breweries.component';
import { BeerStylesComponent } from './pages/beer-styles/beer-styles.component';

export const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: '',
        component: ReviewsListComponent,
    },
    {
        path: 'breweries',
        component: BreweriesComponent,
    },
    {
        path: 'styles',
        component: BeerStylesComponent,
    },
];
