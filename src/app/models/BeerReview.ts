import { User } from './User.model';

export type BeerReview = {
    id: number;
    name: string;
    abv: number;
    ibu: number;
    rating: number;
    image: any;
    description: string;
    styleId?: number;
    style: any;
    breweryId?: number;
    brewery?: any;
    createdAt: any;
    updatedAt: any;
    imageId: number;
    userId: number;
    user: User;
};
