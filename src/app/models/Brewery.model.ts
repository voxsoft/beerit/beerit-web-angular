export type Brewery = {
    id: number;
    name: string;
    country: string;
    logoId?: number;
    logo?: any;
    moderated?: boolean;
};
