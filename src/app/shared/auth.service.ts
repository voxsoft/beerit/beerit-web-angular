import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.development';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

type AuthResponse = {
    token: string;
    user: {
        id: number;
        name: string;
        email: string;
        role: 'USER' | 'ADMIN';
        image: {
            url: string;
            id: number;
        };
    };
    expires: number;
};

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    cookieService = inject(CookieService);

    constructor(private http: HttpClient) {}

    signIn(payload: { login: string; password: string }) {
        return this.http
            .post<AuthResponse>(`${environment.apiHost}/auth/login`, payload)
            .pipe(
                map((response) => {
                    if (response && response.token) {
                        this.setApiToken(response.token, response.expires);
                        this.setUser(response.user);
                        return { authenticated: true };
                    } else {
                        this.setApiToken(null);
                        this.setUser(null);
                        return { authenticated: false };
                    }
                }),
            );
    }

    signUp(payload: { name: string; login: string; password: string }) {
        return this.http
            .post<AuthResponse>(`${environment.apiHost}/auth/signup`, payload)
            .pipe(
                map((response) => {
                    if (response && response.token) {
                        this.setApiToken(response.token, response.expires);
                        this.setUser(response.user);
                        return { authenticated: true };
                    } else {
                        this.setApiToken(null);
                        this.setUser(null);
                        return { authenticated: false };
                    }
                }),
            );
    }

    signOut() {
        return this.http
            .post<any>(`${environment.apiHost}/auth/logout`, {})
            .pipe(
                map(() => {
                    this.setApiToken(null);
                    this.setUser(null);
                    return { authenticated: false };
                }),
            );
    }

    refreshToken() {
        return this.http
            .post<AuthResponse>(`${environment.apiHost}/auth/refresh`, {})
            .pipe(
                map((response) => {
                    if (response && response.token) {
                        this.setApiToken(response.token, response.expires);
                        this.setUser(response.user);
                        return { token: response.token };
                    } else {
                        this.signOut().subscribe();
                        return { token: null };
                    }
                }),
            );
    }

    getUser() {
        const userStr = this.cookieService.get('user');
        if (userStr) {
            try {
                return JSON.parse(userStr);
            } catch (err) {
                return null;
            }
        } else {
            return null;
        }
    }

    setUser(user: Object | null) {
        if (user) {
            this.cookieService.set('user', JSON.stringify(user));
        } else {
            this.cookieService.delete('user');
        }
    }

    setApiToken(token: string | null, expires?: string | number | null) {
        if (token && expires) {
            this.cookieService.set('access_token', token);
            this.cookieService.set('access_token_exp', '' + expires);
        } else {
            this.cookieService.delete('access_token');
            this.cookieService.delete('access_token_exp');
        }
    }

    getApiToken() {
        const token = this.cookieService.get('access_token');
        const expires = this.cookieService.get('access_token_exp');
        return { token, expires: expires ? +expires : null };
    }

    isAuthenticated() {
        const { token, expires } = this.getApiToken();
        if (token === null || expires === null) {
            return false;
        } else if (expires <= Date.now() / 1000) {
            return false;
        } else {
            return true;
        }
    }
}
