import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
    constructor(private authService: AuthService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const {token, expires} = this.authService.getApiToken();
        if (token === null || expires === null) {
            return next.handle(req);
        } else if (expires <= Date.now() / 1000) {
            return next.handle(req);
        } else if (expires <= Date.now() / 1000 + 60 * 60 * 12) {
            // expires in less than 12 hours
            return this.authService.refreshToken().pipe(
                switchMap((tokenRes) => {
                    if (tokenRes.token) {
                        const authReq = req.clone({
                            headers: req.headers.set('Authorization', `Bearer ${tokenRes.token}`),
                        });
                        return next.handle(authReq);
                    } else {
                        return next.handle(req);
                    }
                })
            )
        } else {
            const authReq = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${token}`),
            });
            return next.handle(authReq);
        }
    }
}
