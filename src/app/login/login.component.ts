import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import {
    FormControl,
    FormGroup,
    FormsModule,
    ReactiveFormsModule,
    Validators,
} from '@angular/forms';
import { AuthService } from '../shared/auth.service';

@Component({
    selector: 'app-login',
    standalone: true,
    imports: [CommonModule, FormsModule, ReactiveFormsModule],
    templateUrl: './login.component.html',
    styleUrl: './login.component.scss',
})
export class LoginComponent {
    private authService = inject(AuthService);
    authType = 'login';

    loginForm = new FormGroup({
        login: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
    });

    registrationForm = new FormGroup({
        name: new FormControl('', Validators.required),
        login: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
    });

    changeAuthType() {
        this.authType = this.authType === 'login' ? 'registration' : 'login';
    }

    signIn() {
        if (!this.loginForm.valid) return;
        const payload = {...this.loginForm.value} as {login: string, password: string};
        this.authService.signIn(payload).subscribe({
            next: (data) => console.log(data),
            error: (error) => console.log(error),
        });
    }

    signUp() {
        if (!this.registrationForm.valid) return;
        const payload = {...this.registrationForm.value} as {name: string, login: string, password: string};
        this.authService.signUp(payload).subscribe({
            next: (data) => console.log(data),
            error: (error) => console.log(error),
        });
    }
}
