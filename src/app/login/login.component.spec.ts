import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { provideRouter } from '@angular/router';
import { AuthService } from '../shared/auth.service';

const mockAuthService = {};

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthService;

  beforeEach(async () => {
    const AuthServiceSpy = jasmine.createSpyObj('AuthService', ['signIn', 'signUp']);
    // const result = ReviewsServiceSpy.getReviews.and.returnValue(of({items: [], totalPages: 0}));
    
    await TestBed.configureTestingModule({
      imports: [LoginComponent],
      providers: [provideRouter([])]
    }).overrideComponent(LoginComponent, {
      set: { providers: [{ 
        provide: AuthService, 
        useValue: AuthServiceSpy 
      }]}
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    authService = fixture.debugElement.injector.get(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
